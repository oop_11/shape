/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeproject;

/**
 *
 * @author Admin
 */
public class Squareside {
     private double s;
    
    public Squareside(double s){
        this.s =s;
    }
    public double calArea(){
        return s*s;
    }
    public double getS(){
        return s;
    }
    public void setS(double s){
        if(s<=0){
            System.out.println("Error: Side must more than zero!!!");
            return;
        }
        this.s=s;
    }
     @Override 
     public String toString(){
        return "Area of squareside( s = " +this.getS()+")"+ this.calArea();
        
    }
}
