/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeproject;

/**
 *
 * @author Admin
 */
public class Square {
    private double wide,length;
    public Square(double wide,double length){
        this.wide=wide;
        this.length=length;
        
    }
    public double calArea(){
        return wide*length;
    }
    public void setBoth(double wide,double length){
         if(wide<=0||length<=0){
            System.out.println("Error: wide and length must more than zero!!!");
            return;
        }
        this.wide=wide;
        this.length=length;
    }
     public double getWide(){
        return wide ;
   }
     public double getLength(){
        return length ;
   }
     @Override 
      public String toString(){
        return "Area of square( wide = " +this.getWide()+","+"length = " +this.getLength()+")"+ this.calArea();
        
    }
}
