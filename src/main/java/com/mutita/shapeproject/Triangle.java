/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.shapeproject;

/**
 *
 * @author Admin
 */
public class Triangle {
    private double high,base;
    public static final double num = 1.0/2;
    public Triangle (double high,double base){
        this.high=high;
        this.base=base;
        
    }
    public double calArea(){
        return num*high*base;
    }
    public void setBoth(double high,double base){
        if(high<=0||base<=0){
            System.out.println("Error: high and base must more than zero!!!");
            return;
        }
        this.base=base;
        this.high=high;
        
    }
    public double getBase(){
        return base;
    }
    public double getHigh(){
        return high;
    }
    @Override 
     public String toString(){
       return "Area of Triangle( base = " +this.getBase()+",high = "+this.getHigh()+")"+ this.calArea();
     }
}
